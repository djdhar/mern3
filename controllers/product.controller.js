const Product = require('../models/product.model');

exports.test = function (req, res) {
    res.send('Hello Testing!');
};

exports.product_create = function (req, res) {
    let product = new Product(
        {
            name: req.body.name,
            price: req.body.price
        }
    );

    product.save().then(()=>{
        res.json(product)
    }).catch((err)=>{
        res.send(err)
    })
};

exports.product_details = function (req, res) {
    Product.findById(req.params.id).then((product)=>{
        res.json(product)
    }).catch((err)=>{
        res.send(err)
    })
};


exports.product_update = function (req, res) {
    Product.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true})
    .then((product)=>{
        res.json(product)
    }).catch((err)=>{
        res.send(err)
    })
};

exports.product_delete = function (req, res) {
    Product.findByIdAndRemove(req.params.id)
    .then((product)=>{
        res.json(product)
    }).catch((err)=>{
        res.send(err)
    })
};