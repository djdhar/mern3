const express = require('express');
const bodyParser = require('body-parser');

const app = express();

const product = require('./routes/product.route'); 

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use('/products', product);

let port = 3001;

const mongoose = require('mongoose');
let dev_db_url = 'mongodb://localhost:27017';
let mongoDB = process.env.MONGODB_URI || dev_db_url;
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));


app.listen(port, () => {
    console.log('Server is up and running on port number ' + port);
});